/* TODO:

*/

var inputGroup = {
"chNetworkFolder": {
	"prompt": " Create S.O. folder on network",
	"onParts": true,
	"onMandrel": true
	},
"chPrintTDS": {
	"prompt": " Print TDS",
	"onParts": false,
	"onMandrel": true
	},
"chPanInfo": {
	"prompt": " Obtain pan information",
	"onParts": true,
	"onMandrel": true
	},
"chCheckDrawingExists": {
	"prompt":  " Check if drawing extists",
	"onParts": true,
	"onMandrel": true
	},
"chCreateDrawings": {
	"prompt":  " Create component drawings",
	"onParts": true,
	"onMandrel": true
	},
"chTicketDrawings": {
	"prompt": " Attach drawings to ticket",
	"onParts": true,
	"onMandrel": true
	},
"chCreateECN": {
	"prompt": " Create ECN",
	"onParts": true,
	"onMandrel": false
	},
"chTicketECN": {
	"prompt": " Attach ECN to ticket",
	"onParts": true,
	"onMandrel": false
	},
"chHistory": {
	"prompt": " Enter record into history database",
	"onParts": true,
	"onMandrel": true
	},
"chWritePNs": {
	"prompt": " Write P/Ns on SO",
	"onParts": true,
	"onMandrel": true
	},
"chScan": {
	"prompt": " Scan necessary documents ",
	"onParts": true,
	"onMandrel": true
	},
"chEmailDataEntry": {
	"prompt": " Email drawings to data entry ",
	"onParts": true,
	"onMandrel": true
	},
"chEmailInsp": {
	"prompt": " Email scan to Inspection",
	"onParts": true,
	"onMandrel": true
	},
"chCloseTicket": {
	"prompt": " Assign ticket to data entry and post a request for closing ",
	"onParts": true,
	"onMandrel": true
	},
"chForgot": {
	"prompt": " Do something you probably forgot",
	"onParts": true,
	"onMandrel": true
	},
"chRegret": {
	"onParts": true,
	"prompt": " Regret you forgot to do that thing and apologize to everyone",
	"onMandrel": true
	}
};


function generateChecklist(params) {
	$.each(inputGroup, function(key, value){
		var chHtml = "\
<div class=\"checkbox\"><label>\
<input class=\"SOForm\" type=\"checkbox\" id=\"" + key + "\">" + value.prompt + "\
</label></div>";
	$('#checkboxList').append(chHtml);
	
	// put horizontal line after controll 'create drawings'
	// line will signify having drawing checked
	if (key === 'chCreateDrawings') {
		$('#checkboxList').append("<hr>");
	}
	
	});
}

function contextChecklist() {
    var isMachineOrder = ($('#selOrderType').val() === "Machine");
    var isMandrelOrder = ($('#selModelType').val() === "Seeder");
    
	var type = (isMachineOrder ? 'Machine' : 'Part');
	var seed = (isMandrelOrder ? 'Seeder' : 'Non-seeder');
	console.log(type + ' | ' + seed);
	
	// dumb but try it
	$.each($('#checkboxList .checkbox'), function() {
		var thisID = $('.SOForm', this).attr('id');
		var thisOnMandrel = inputGroup[thisID].onMandrel;
		var thisOnParts = inputGroup[thisID].onParts
		
		$(this).hide();
		
		// only shows control if order conditions allow		
		if (isMandrelOrder && !thisOnMandrel) {
			return true;
		}
		if (!isMachineOrder && !thisOnParts) {
			return true;
		}
		$(this).show();
	});
}