/* TODO:
See about a new page showing progress bars of each order from 
an advanced options menu

look into having a delete alert and providing an undo option like:
"Deleted Order 123123 Undo" with 'undo' being a link to place 
deleted order back into the list

disable saving empty record and warn not to do that

done - make checkboxes contextual to order type 
done - fill checkboxes from object after contextualizing the form 
*/

var currentOrder = '';
var deletedOrder = {};

// disgusting function to insert saved message
function showAlert(){
    $('.alert-save').remove();
	console.log("Current order is " + currentOrder);
    
    var alert = "<div class=\"alert alert-success\
    alert-dismissible alert-save\" role=\"alert\"><button type=\"button\" class=\
    \"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span\
    aria-hidden=\"true\" class=\"alert-close\">&times;</span></button>\
    Saved " + currentOrder + "</div>";
    $('#alertSpace').append(alert);
}

function showDeleteAlert() {
    $('.alert-delete').remove();
	console.log("Current order is " + currentOrder);
    
    var alert = "<div class=\"alert alert-info\
    alert-dismissible alert-delete\" role=\"alert\"><button type=\"button\" class=\
    \"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span\
    aria-hidden=\"true\" class=\"alert-close\">&times;</span></button>\
    Deleted " + deletedOrder.tbSONo +  " <a id=\"undoDelete\" href=\"#\" class=\"alert-link\"> Undo</a></div>";
    $('#alertSpace').append(alert);
    
    $('#undoDelete').click(function(){
        undoDeleteOrder();
        $('.alert-delete').remove();
    });
}

    
function saveOrder() {
    // 'open' localStorage for manipulation
	currentOrder = $('#tbSONo').val();
	var orders = JSON.parse(localStorage.orders);
    var order = {};
    var formElements = $(".SOForm");

    $.each(formElements, function(){

        var thisID = $(this).attr('id');

        switch (thisID) {
			// textboxes
            case "tbSONo":
            case "tbCust":
            case "tbSN":
            case "selOrderType":
            case "selModelType":
                order[thisID] = $(this).val();
                break;

            case "chNetworkFolder":
            case "chPrintTDS":
            case "chPanInfo":
            case "chCheckDrawingExists":
            case "chCreateDrawings":
            case "chTicketDrawings":
            case "chCreateECN":
            case "chTicketECN":
            case "chHistory":
            case "chWritePNs":
            case "chScan":
            case "chEmailDataEntry":
            case "chEmailInsp":
            case "chCloseTicket":
            case "chForgot":
            case "chRegret":
                order[thisID] = $(this).prop("checked");
				break;
        }
    });
    console.log(order);
    orders[order.tbSONo] = order;
    localStorage.setItem('orders', JSON.stringify(orders));
    
    // show saved message
	showAlert();
    fillOrderList();
}

$("#btnSave").click(saveOrder);

// see storage
//$("#storage").click(function(){
function showStorage() {
	console.log("Showing storage:");
    $.each(JSON.parse(localStorage.orders), function(key, value) {
		console.log(localStorage[key]);
	});
}

// check or uncheck
$("#checkAll, #uncheckAll").click(function(){
	console.log($(this).attr('id') == "checkAll");
	if ($(this).attr('id') == "checkAll") {
		var check = true;
	} else {
		var check = false;
	}
	var checkBoxes = $(".SOform,:checkbox");
	$.each(checkBoxes, function(){
		$(this).prop('checked', check);
    });
});

// fills the form with argued order
function getOrder(orderNo) {
    // hide alerts
    $('.alert-save').hide();
	var order = JSON.parse(localStorage.orders)[orderNo];
	if (order === undefined) {
		alert('Order not found');
	} else {
		$.each(order, function(key, value) {
            switch (key.slice(0,2)) {
                // case data is a textbox (tb)
                // or select (se)
                case 'tb':
                case 'se':
                    $('#' + key).val(value);
                    break;
                case 'ch':
                    $('#' + key).prop('checked', value);
                    break;
            }
		});
	}
    contextChecklist();
}

// fills dropdown with data and adds dropdown item functionallity
function fillOrderList() {
    $('#orderList').empty();
	var orders = JSON.parse(localStorage.orders);
	for (var iOrder in orders) {
        //if mandrel order, don't show ECN checkboxes
		$('#orderList').append("<li id=\"" + iOrder + "\"><a class=\"" + iOrder + "\" href='#'>" + iOrder + "</a></li>");
	}
    $('ul#orderList li a').click(function(){
        //assigning global to picked order
        currentOrder = $(this).text()
		console.log(currentOrder);
        getOrder(currentOrder);
	});
};

function clearForm() {
    $.each($('.SOForm'), function(){
        $(this).prop('checked', false);
        $(this).val("");
    });
}

function newOrder(orderNumber) {
    clearForm();
    currentOrder = '';
}

function deleteCurrentOrder(){
    console.log('Attempting to delete: ' + currentOrder);
    clearForm();
    orders = JSON.parse(localStorage.orders);
    
    deletedOrder = orders[currentOrder];
    
    delete orders[currentOrder];
    localStorage.setItem('orders', JSON.stringify(orders));
    currentOrder = '';
    fillOrderList();
    showDeleteAlert();
}

function undoDeleteOrder() {
    console.log('Trying to undo your mistake...');
    orders = JSON.parse(localStorage.orders);
    orders[deletedOrder.tbSONo] = deletedOrder;
    localStorage.setItem('orders', JSON.stringify(orders));
    fillOrderList();
    getOrder(deletedOrder.tbSONo);
    deletedOrder = {};
}

$('#someLink').click(function(){
    alert("Something");
})

$('#selModelType, #selOrderType').change(function(){
    contextChecklist();
});

$('#newOrder').click(newOrder);

$('#deleteOrder').click(deleteCurrentOrder);

// $('#undoDelete').click(undoDeleteOrder);
$('a.alert-link').click(function(event) {
    event.preventDefault();
    alert('hi. wanna undo something?');
});

$(function(){
    
	if (localStorage.orders === undefined) {
		localStorage.setItem('orders', '{}');
	}
    clearForm();
    fillOrderList();
	
	// from inputs.js
	generateChecklist();
	
	// to fill checkboxes on refresh
    //currentOrder = $('#tbSONo').val()
	//getOrder(currentOrder);
	
    console.log('greetings');
});

