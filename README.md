# Sales Order Checklist #

SO Checklist is a checklist web app for tracking progress of routine tasks. 

### Features ###

* Keep track of multiple records at once.
* Persistent data held in the browser localStorage variable as JSON.

### To Preview Functionality ###

* Just clone or download and open index.html!